export type Vendor = {
  data: {
    address: string;
    backgroundImage: string;
    title: string;
    voteCount: number;
    rate: number;
    description: string;
    deliver: boolean;
    deliveryFee: number;
    defLogo: string;
  };
};

export type VendorsListResponse = {
  data: {
    finalResult: Vendor[];
    count: number;
  };
};
