import Image from "next/image";
import styles from "./page.module.scss";
import Link from "next/link";

export default function Home() {
  return (
    <div className={styles.main}>
      <Link href="/vendors" className={`${styles.link_five} ${styles.attlink}`}>
        نمایش لیست رستوران ها
      </Link>
    </div>
  );
}
