import { Providers } from "@/redux/provider";
import "./globals.scss";
import type { Metadata } from "next";
import localFont from "next/font/local";

export const metadata: Metadata = {
  title: "Vendors List",
  description: "Test for list of vendors",
};

const yekanFont = localFont({ src: "../../public/fonts/Yekan.ttf" });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="fa">
      <body className={yekanFont.className}>
        <main>
          <Providers>{children}</Providers>
        </main>
      </body>
    </html>
  );
}
