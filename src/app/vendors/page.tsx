"use client";

import { VendorsList } from "@/components";

const Vendors = () => {
  return <VendorsList />;
};

export default Vendors;
