import { VendorsListResponse } from "@/types/Vendor";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const vendorsApi = createApi({
  reducerPath: "vendorsApi",
  refetchOnFocus: false,
  baseQuery: fetchBaseQuery({
    baseUrl: "https://snappfood.ir/mobile/v3",
  }),

  endpoints: (builder) => ({
    getVendors: builder.query<VendorsListResponse, number>({
      query: (page = 1) => {
        const params = new URLSearchParams({
          page: page.toString(),
          page_size: "15",
          lat: "35.735",
          long: "51.521",
        });
        return `/restaurant/vendors-list?${params}`;
      },
    }),
  }),
});

export const { useGetVendorsQuery } = vendorsApi;
