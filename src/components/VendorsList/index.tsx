import React, { useState, useEffect, useRef } from "react";
import { useVirtualizer } from "@tanstack/react-virtual";
import { useGetVendorsQuery } from "@/redux/services/vendorsApi";
import styles from "./VendorsList.module.scss";
import { Vendor } from "../Vendor";
import { Vendor as VendorType } from "@/types/Vendor";

export const VendorsList = () => {
  const [page, setPage] = useState(1);
  const [vendors, setVendors] = useState<VendorType[]>([]);

  const { data, isLoading, isFetching } = useGetVendorsQuery(page);
  const parentRef = useRef<HTMLDivElement>(null);

  const rowVirtualizer = useVirtualizer({
    count: vendors.length || 0,
    getScrollElement: () => parentRef.current,
    estimateSize: () => 275,
    overscan: 5,
  });

  useEffect(() => {
    window.addEventListener("scroll", windowOnScroll);

    return () => {
      window.removeEventListener("scroll", windowOnScroll);
    };
  }, []);

  useEffect(() => {
    if (data && data.data && !isFetching) {
      setVendors((prev) => prev?.concat(data.data.finalResult));
    }
  }, [page, isFetching]);

  const windowOnScroll = () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      setPage((prev) => prev + 1);
    }
  };

  if (isLoading) {
    return null;
  }

  return (
    <div className={styles.main} ref={parentRef}>
      <div
        style={{
          height: `${rowVirtualizer.getTotalSize()}px`,
          width: "100%",
          position: "relative",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        {vendors?.length &&
          rowVirtualizer
            .getVirtualItems()
            .map((virtualItem) => (
              <Vendor item={vendors[virtualItem.index]} key={virtualItem.key} />
            ))}
        {isFetching && <p>در حال بارگزاری</p>}
      </div>
    </div>
  );
};
