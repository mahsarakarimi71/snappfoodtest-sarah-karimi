import React from "react";
import styles from "./VendorDetail.module.scss";

type VendorDetailProps = React.ComponentProps<"div">;
type RowProps = {
  name?: string;
} & React.ComponentProps<"div">;

export const VendorDetail = ({ children }: VendorDetailProps) => {
  return <div className={styles.main}>{children}</div>;
};

const Row = ({ name, children, ...restProps }: RowProps) => {
  return (
    <div {...restProps} className={styles.row}>
      {/* <h4>{name}</h4> */}
      {children}
    </div>
  );
};

VendorDetail.Row = Row;
