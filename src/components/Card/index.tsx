import React from "react";
import styles from "./card.module.scss";
import { GeneralType } from "@/types";

type CardProps = React.ComponentProps<"div">;

export const Card = ({ children, ...restProps }: CardProps) => {
  return (
    <div {...restProps} className={styles.card}>
      {children}
    </div>
  );
};

const Header = ({ children, ...restProps }: GeneralType) => {
  return (
    <div {...restProps} className={styles.header}>
      {children}
    </div>
  );
};

const Item = ({ children, ...restProps }: GeneralType) => {
  return (
    <div {...restProps} className={styles.item}>
      {children}
    </div>
  );
};

const Logo = ({ children, ...restProps }: GeneralType) => {
  return (
    <div {...restProps} className={styles.logo}>
      <div className={styles.imageOutline}>{children}</div>
    </div>
  );
};

Card.Header = Header;
Card.Item = Item;
Card.Logo = Logo;
