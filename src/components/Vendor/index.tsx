import Image from "next/image";
import { Card, StarIcon, VendorDetail } from "..";
import { Vendor as VendorType } from "@/types/Vendor";
import styles from "./Vendor.module.scss";
import { ToRial } from "@/lib";

type VendorProps = {
  item: VendorType;
  key: any;
};

export const Vendor = ({ item, key }: VendorProps) => {
  return (
    <Card key={key}>
      <Card.Header>
        <Image
          src={item.data.backgroundImage || ""}
          alt="background"
          width={0}
          height={0}
          sizes="100vw"
          style={{ width: "100%", height: "auto", objectFit: "cover" }}
        />
      </Card.Header>
      <Card.Logo>
        <Image
          src={item.data.defLogo || ""}
          alt="logo"
          width={60}
          height={60}
          // sizes="820px"
          objectFit="cover"
          // style={{ width: "100%", height: "auto", objectFit: "cover" }}
        />
      </Card.Logo>
      <Card.Item>
        <VendorDetail>
          <VendorDetail.Row>
            <div>
              <h4>{item.data.title}</h4>
            </div>
            <div>
              <VendorDetail.Row>
                <span className={styles.vote}>({item.data.voteCount})</span>
                <div className={styles.divRate}>
                  <span className={styles.rate}>{item.data.rate}</span>
                  <StarIcon />
                </div>
              </VendorDetail.Row>
            </div>
          </VendorDetail.Row>
          <VendorDetail.Row>
            <div>
              <span className={styles.des}>
                {item.data.description || "خشکبار، آجیل، میوه خشک"}
              </span>
            </div>
          </VendorDetail.Row>
          <VendorDetail.Row>
            <div className={styles.divDelivery}>
              <span className={styles.delivery}>
                {item.data.deliver ? "پیک فروشنده" : "ارسال اکسپرس"}
              </span>
              <div className={styles.divPrice}>
                <span className={styles.price}>
                  {ToRial(item.data.deliveryFee.toString() || "")}
                </span>
                <span className={styles.currency}>تومان</span>
              </div>
            </div>
          </VendorDetail.Row>
        </VendorDetail>
      </Card.Item>
    </Card>
  );
};
